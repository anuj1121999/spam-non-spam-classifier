# README #

The project is used to classify mails as spam or non spam using Machine Learning.
Various algorithms have been implemented such as MultinomialNB,LogisticRegression,SVM.

roc score ~ 0.97
Libraries used:sklearn,numpy,pandas.